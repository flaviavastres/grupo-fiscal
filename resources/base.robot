*Settings*
Documentation                Novo cadastro de grupo fiscal do ZAK

Library                      Browser

Resource                     actions/source.robot

*Keywords*
Start Session
    New Browser              chromium             headless=false
    New Context              viewport={'width': 1366, 'height': 768}    
    New Page                 https://zak-dashboard.stg.mimic.com.br/login
    Get Text                 h1                   contains                Gerenciamento de restaurantes