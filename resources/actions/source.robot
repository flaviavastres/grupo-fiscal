*Settings*
Documentation                   Cadastro de grupo fiscal do ZAK

*Variables*
${groupName}                    outback
${username}                     quality
${password}                     qa1234

*Keywords*
Validar o cadastro de grupo fiscal da organização
#Login
    Fill text                   id=groupName                    ${groupName}        
    Fill text                   id=username                     ${username}
    Fill text                   id=password                     ${password}
      
    Click                       id=submitButton
    
    Click                       text=Outback
    Click                       text=Zak

#Sleep usado exclusivamente para que o range de ORGANIZAÇÃO esteja ativo para o clique
   Sleep                        2
                                                                                
#Acesso a aba de ORGANIZAÇÃO para criar um novo grupo fiscal
    Click                       text=ORGANIZAÇÃO
    Click                       text=Cardápio
    Click                       text=Grupo Fiscal 
    Click                       text=Criar Grupo Fiscal       

#Nome do grupo fiscal
    Fill text                   xpath=//span[text()="Nome do Grupo Fiscal"]               Teste de grupo fiscal do Zak
#CFOP
    Fill text                   xpath=//span[text()="CFOP"]                               5104
#CST do ICMS 
    Fill text                   id=cstIcms                                                40        
#CSOSN do ICMS
    Fill text                   xpath=//span[text()="CSOSN"]                              500
#ALÍQUOTA do ICMS
    Fill text                   id=aliquotaPercIcms                                       3
#Abrindo o modal do dropdown
    Click                       id=icmsOrigin
#Seleção de uma das opções de origem 
    Click                       xpath=//li[contains(text(),'0 - Nacional')]                                                                         
#CST do PIS
    Fill text                   id=cstPis                                                 02
#Alíquota do PIS
    Fill text                   id=aliquotaPercPis                                        3
#CST do COFINS
    Fill text                   id=cstCofins                                              01
#Alíquota do COFINS 
    Fill text                   id=aliquotaPercCofins                                     2

    Click                       text=SALVAR 

Validar o search do grupo fiscal criado
#Login
    Fill text                   id=groupName                    ${groupName}        
    Fill text                   id=username                     ${username}
    Fill text                   id=password                     ${password}
    
    Click                       id=submitButton
    
    Click                       text=Outback
    Click                       text=Zak

#Sleep usado exclusivamente para que o range de ORGANIZAÇÃO esteja ativo para o clique
   Sleep                        2

#Acesso a aba de ORGANIZAÇÃO para procurar o grupo fiscal criado
    Click                       text=ORGANIZAÇÃO
    Click                       text=Cardápio
    Click                       text=Grupo Fiscal 

#Search
    Fill text                   css=input[placeholder="Procurando algo?"]                   Teste de grupo fiscal do Zak